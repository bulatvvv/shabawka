from general import general


class Car(general):
    def __init__(self, code=0, mark="default", price=0, rental_price=0, type="default"):
        general.__init__(self, code)
        self.setMark(mark)
        self.setPrice(price)
        self.setRentalPrice(rental_price)
        self.setType(type)

    def setMark(self, mark):
        self.mark = mark

    def setPrice(self, price):
        self.price = price

    def setRentalPrice(self, rental_price):
        self.rental_price = rental_price

    def setType(self, type):
        self.type = type

    def getMark(self):
        return self.mark

    def getPrice(self):
        return self.price

    def getRentalPrice(self):
        return self.rental_price

    def getType(self):
        return self.type
