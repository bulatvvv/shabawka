from DealsList import DealsList
from generalListEdit import generalListEdit
from Deal import Deal


class DealsListEdit(DealsList, generalListEdit):
    def newRec(self, code=0, client='', car='', datefrom='', period='', active=''):
        self.appendList(Deal(code, client, car, datefrom, period, active))

    def setClient(self, code, value):
        self.findByCode(code).setClient(value)

    def setCar(self, code, value):
        self.findByCode(code).setCar(value)

    def setDateFrom(self, code, value):
        self.findByCode(code).setDateFrom(value)

    def setPeriod(self, code, value):
        self.findByCode(code).setPeriod(value)

    def setActive(self, code, value):
        self.findByCode(code).setActive(value)
