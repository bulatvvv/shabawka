from generalList import generalList


class CarList(generalList):
    def __init__(self):
        generalList.__init__(self)

    def getMark(self, code):
        return self.findByCode(code).getMark()

    def getPrice(self, code):
        return self.findByCode(code).getPrice()

    def getType(self, code):
        return self.findByCode(code).getType()

    def getRentalPrice(self, code):
        return self.findByCode(code).getRentalPrice()
