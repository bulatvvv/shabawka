from Client import Client
from generalList import generalList


class DealsList(Client, generalList):
    def __init__(self, discount_threshold=10):
        generalList.__init__(self)
        # Client.__init__(self) #какая то хуйня, здесь надо разобраться
        self.discountThreshold = discount_threshold

    def addDeal(self, deal):
        self.__list.append(deal)

    def endDeal(self, deal_code, curdate):
        deal = self.findDeal(deal_code).setActive(False)
        price = deal.getCar().getPrice() * deal.getPeriod()
        if self.isGoldClient(deal.getClient()):
            price *= 0.85

        actual_rental_time = (deal.getDateFrom() - curdate).days
        if actual_rental_time > deal.getPeriod():
            price = price + (actual_rental_time - deal.getPeriod()) * deal.getCar().getPrice()
            price *= 1.2  # ШТРАФ

        return price

    def _getDealsPerClient(self, client: Client):
        return len([x for x in self.__list if x.client.getCode() == client.getCode()])

    def isGoldClient(self, client: Client):
        return self._getDealsPerClient(client) > self.discountThreshold

    def findDeal(self, deal_code):
        for i in self.__list:
            if i.code == deal_code:
                return i

    def getClient(self, code):
        return self.findByCode(code).getClient()

    def getCar(self, code):
        return self.findByCode(code).getCar()

    def getDateFrom(self, code):
        return self.findByCode(code).getDateFrom()

    def getPeriod(self, code):
        return self.findByCode(code).getPeriod()

    def isActive(self, code):
        return self.findByCode(code).isActive()
