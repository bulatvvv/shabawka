from ClientList import ClientList
from generalListEdit import generalListEdit
from Client import Client


class ClientListEdit(ClientList, generalListEdit):
    def newRec(self, code=0, surname='', name='', middlename='', address='', phone=''):
        self.appendList(Client(code, surname, name, middlename, address, phone))

    def setSurname(self, code, value):
        self.findByCode(code).setSurname(value)

    def setName(self, code, value):
        self.findByCode(code).setName(value)

    def setMiddleName(self, code, value):
        self.findByCode(code).setMiddleName(value)

    def setAddress(self, code, value):
        self.findByCode(code).setAddress(value)

    def setPhone(self, code, value):
        self.findByCode(code).setPhone(value)
