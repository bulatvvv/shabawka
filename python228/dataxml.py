import os, xml.dom.minidom


class dataxml:
    def read(self, inp, rent):
        dom = xml.dom.minidom.parse(inp)
        dom.normalize()
        for node in dom.childNodes[0].childNodes:
            if (node.nodeType == node.ELEMENT_NODE) and (node.nodeName == "car"):
                code, mark, price, rentalprice, type = 0, "", 0, 0, ""
                for t in node.attributes.items():
                    if t[0] == "code": code = int(t[1])
                    if t[0] == "mark": mark = (t[1])
                    if t[0] == "price": price = int(t[1])
                    if t[0] == "rentalprice": rentalprice = int(t[1])
                    if t[0] == "type": type = (t[1])
                rent.newCar(code, mark, price, rentalprice, type)
            if (node.nodeType == node.ELEMENT_NODE) and (node.nodeName == "client"):
                code, name, surname, middlename, address, phone = 0, "", "", "", "", ""
                for t in node.attributes.items():
                    if t[0] == "code": code = int(t[1])
                    if t[0] == "name": name = (t[1])
                    if t[0] == "surname": surname = (t[1])
                    if t[0] == "middlename": middlename = (t[1])
                    if t[0] == "address": address = (t[1])
                    if t[0] == "phone": phone = (t[1])
                rent.newClient(code, name, surname, middlename, address, phone)
            if (node.nodeType == node.ELEMENT_NODE) and (node.nodeName == "deal"):
                code, client, car, datefrom, period, active = 0, "", "", "", 0, ""
                for t in node.attributes.items():
                    if t[0] == "code": code = int(t[1])
                    if t[0] == "client": client = (t[1])
                    if t[0] == "car": car = (t[1])
                    if t[0] == "datefrom": datefrom = (t[1])
                    if t[0] == "period": period = int(t[1])
                    if t[0] == "active": active = (t[1])
                rent.newDeal(code, client, car, datefrom, period, active)

    def write(self, out, rent):
        dom = xml.dom.minidom.Document()
        root = dom.createElement("rental")
        dom.appendChild(root)
        for c in rent.getCarCodes():
            car = dom.createElement("car")
            car.setAttribute('code', str(c))
            car.setAttribute('mark', rent.getCarMark(c))
            car.setAttribute('price', rent.getCarPrice(c))
            car.setAttribute('rentalprice', rent.getCarRentalPrice(c))
            car.setAttribute('type', rent.getCarType(c))
        for c in rent.getClientCodes():
            cl = dom.createElement("client")
            cl.setAttribute('code', str(c))
            cl.setAttribute('name', rent.getClientName(c))
            cl.setAttribute('surname', rent.getClientSurname(c))
            cl.setAttribute('middlename', rent.getClientMiddleName(c))
            cl.setAttribute('address', rent.getAddress(c))
            cl.setAttribute('phone', rent.getPhone(c))
        for c in rent.getDealCodes():
            deal = dom.createElement("deals")
            deal.setAttribute('code', str(c))
            deal.setAttribute('client', rent.getDealClient(c))
            deal.setAttribute('car', rent.getDealCar(c))
            deal.setAttribute('datefrom', rent.getDealDateFrom(c))
            deal.setAttribute('period', rent.getDealPeriod(c))
            deal.setAttribute('active', rent.getDealActive(c))
        f = open(out, 'w')
        s = dom.toprettyxml(encoding='utf-8')

        f.write(s.decode('utf-8'))




