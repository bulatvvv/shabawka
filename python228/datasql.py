import os
import sqlite3 as db

emptydb = """
PRAGMA foreign_keys = ON;

create table car
(code integer primary key,
 mark text,
 price integer,
 rentalprice integer,
 type text
 );

create table client
(code integer primary key,
 name text,
 surname text,
 middlename text,
 address text,
 phone text
);

create table deal
(code integer primary key
 client text,
 car text,
 datefrom text,
 period integer,
 active integer
);
"""


class datasql:
    def read(self, inp, rent):
        conn = db.connect(inp)
        curs = conn.cursor()
        curs.execute("select code, mark, price, rentalprice, type from car")
        data = curs.fetchall()
        for r in data: rent.newCar(r[0], r[1], r[2], r[3], r[4])
        curs.execute("select code, name, surname, middlename, address, phone from client")
        data = curs.fetchall()
        for r in data: rent.newClient(r[0], r[1], r[2], r[3], r[4], r[5])
        curs.execute("select code, client, car, datefrom, period, active from deal")
        data = curs.fetchall()
        for r in data: rent.newDeal(r[0], r[1], r[2], r[3], r[4], r[5])
        conn.close()

    def write(self, out, rent):
        conn = db.connect(out)
        curs = conn.cursor()
        curs.executescript(emptydb)
        for c in rent.getCarCodes():
            curs.execute("insert into car(code, mark, price, rentalprice, type) "
                         "values('%s', '%s', '%s','%s','%s' )"
                         %(str(c),
                           rent.getCarMark(c),
                           rent.getCarPrice(c),
                           rent.getCarRentalPrice(c),
                           rent.getCarType(c)
                           ))
        for c in rent.getClientCodes():
            curs.execute("insert into client(code, name, surname, middlename, address, phone) "
                         "values('%s', '%s', '%s','%s','%s', '%s')"
                         %(str(c),
                           rent.getClientName(c),
                           rent.getClientSurname(c),
                           rent.getClientMiddleName(c),
                           rent.getAddress(c),
                           rent.getPhone(c)
                           ))
        for c in rent.getDealCodes():
            curs.execute("insert into deal(code, client, car, datefrom, period, active) "
                         "values('%s', '%s', '%s','%s','%s', '%s')"
                         %(str(c),
                           rent.getDealClient(c),
                           rent.getDealCar(c),
                           rent.getDealDateFrom(c),
                           rent.getDealPeriod(c),
                           rent.getDealActive(c)
                           ))
        conn.commit()
        conn.close()