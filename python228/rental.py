from CarListEdit import CarListEdit
from ClientListEdit import ClientListEdit
from DealsListEdit import DealsListEdit


class rental:
    def __init__(self):
        self.__cars = CarListEdit()
        self.__clients = ClientListEdit()
        self.__deals = DealsListEdit()

    def removeCar(self, code):
        self.__cars.removeList(code)

    def removeClient(self, code):
        self.__clients.removeList(code)

    def removeDeal(self, code):
        self.__deals.removeList(code)

    def clear(self):
        self.__deals.clear()
        self.__clients.clear()
        self.__cars.clear()

    def newCar(self, code=0, mark='', price='', rentalprice='', type=''):
        self.__cars.newRec(code, mark, price, rentalprice, type)

    def findCarByCode(self, code):
        return self.__cars.findByCode(code)

    def getCarNewCode(self):
        return self.__cars.getNewCode()

    def getCarCodes(self):
        return self.__cars.getCodes()

    def getCarMark(self, code):
        return self.__cars.getMark(code)

    def getCarPrice(self, code):
        return self.__cars.getPrice(code)

    def getCarRentalPrice(self, code):
        return self.__cars.getRentalPrice(code)

    def getCarType(self, code):
        return self.__cars.getType(code)

    def setCarMark(self, code, value):
        self.__cars.setMark(code, value)

    def setCarPrice(self, code, value):
        self.__cars.setPrice(code, value)

    def setCarRentalPrice(self, code, value):
        self.__cars.setRentalPrice(code, value)

    def setCarType(self, code, value):
        self.__cars.setType(code, value)

    def newClient(self, code=0, name='', surname='', middlename='', address='', phone=''):
        self.__clients.newRec(code, surname, name, middlename, address, phone)

    def findClientByCode(self, code):
        return self.__clients.findByCode(code)

    def getClientNewCode(self):
        return self.__clients.getNewCode()

    def getClientCodes(self):
        return self.__clients.getCodes()

    def getClientName(self, code):
        return self.__clients.getName(code)

    def getClientSurname(self, code):
        return self.__clients.getSurname(code)

    def getClientMiddleName(self, code):
        return self.__clients.getMiddleName(code)

    def getAddress(self, code):
        return self.__clients.getAddress(code)

    def getPhone(self, code):
        return self.__clients.getPhone(code)

    def newDeal(self, code, client, car, datefrom, period, active):
        self.__deals.newRec(code, client, car, datefrom, period, active)

    def findDealByCode(self, code):
        return self.__deals.findByCode(code)

    def getDealNewCode(self):
        return self.__deals.getNewCode()

    def getDealCodes(self):
        return self.__deals.getCodes()

    def getDealClient(self, code):
        return self.__deals.getClient(code)

    def getDealCar(self, code):
        return self.__deals.getCar(code)

    def getDealDateFrom(self, code):
        return self.__deals.getDateFrom(code)

    def getDealPeriod(self, code):
        return self.__deals.getPeriod(code)

    def getDealActive(self, code):
        return self.__deals.isActive(code)
