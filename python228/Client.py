from general import general


class Client(general):
    def __init__(self, code=0, name='', surname='', middle_name='', address='', phone=''):
        general.__init__(self, code)
        self.setName(name)
        self.setSurname(surname)
        self.setMiddleName(middle_name)
        self.setAddress(address)
        self.setPhone(phone)

    def setName(self, name):
        self.name = name

    def setPhone(self, phone):
        self.phone = phone

    def setSurname(self, surname):
        self.surname = surname

    def setAddress(self, address):
        self.address = address

    def setMiddleName(self, middle_name):
        self.middle_name = middle_name

    def getName(self):
        return self.name

    def getSurname(self):
        return self.surname

    def getMiddleName(self):
        return self.middle_name

    def getAddress(self):
        return self.address

    def getPhone(self):
        return self.phone
