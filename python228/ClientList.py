from generalList import generalList


class ClientList(generalList):
    def __init__(self):
        generalList.__init__(self)

    def getName(self, code):
        return self.findByCode(code).getName()

    def getSurname(self, code):
        return self.findByCode(code).getSurname()

    def getMiddleName(self, code):
        return self.findByCode(code).getMiddleName()

    def getAddress(self, code):
        return self.findByCode(code).getAddress()

    def getPhone(self, code):
        return self.findByCode(code).getPhone()

