from CarList import CarList
from generalListEdit import generalListEdit
from Car import Car


class CarListEdit(CarList, generalListEdit):
    def newRec(self, code=0, mark='', price=0, rentalprice=0, type=''):
        self.appendList(Car(code, mark, price, rentalprice, type))

    def setMark(self, code, value):
        self.findByCode(code).setMark(value)

    def setPrice(self, code, value):
        self.findByCode(code).setPrice(value)

    def setRentalPrice(self, code, value):
        self.findByCode(code).setRentalPrice(value)

    def setType(self, code, value):
        self.findByCode(code).setType(value)
