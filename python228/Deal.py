from general import general


class Deal(general):
    def __init__(self, code=0, client=None, car=None, date_from=None, period=0, active=None):
        general.__init__(self, code)
        self.setClient(client)
        self.setCar(car)
        self.setDateFrom(date_from)
        self.setPeriod(period)
        self.setActive(True)

    def setClient(self, client):
        self.client = client

    def getClient(self):
        return self.client

    def setCar(self, car):
        self.car = car

    def getCar(self):
        return self.car

    def setDateFrom(self, date_from):
        self.date_from = date_from

    def getDateFrom(self):
        return self.date_from

    def setPeriod(self, period):
        self.period = period

    def getPeriod(self):
        return self.period

    def setActive(self, active):
        self.active = active

    def isActive(self):
        return self.active;
